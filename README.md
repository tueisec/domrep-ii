# DOMREP II

For details please mail [Matthias Probst](mailto:matthias.probst@tum.de)

This directory holds the measurement results for the Paper DOMREP-II published at IEEE HOST 2024.
Please not, that the original files have one million traces and result in file sizes of about 170GB. Thus, we only provide 10,000 traces here, but the measurements still expose the same trend as depicted in the paper.

Each file contains some datasets, where the most important are:
* `executable binary` which can be executed on the STM32F071 on the CW308 board. Trigger is connected to GPIO 4, UART connected to TX GPIO1 and RX GPIO2
* `samples_power` containing the measurement samples
* `input` and `fault` sever es distinguisher, depending on the measurement case (i.e masked-no-fault and always fault are distungished by the input, fault-nofault with the fault dataset). The first element containes the elemnent in the fix dataset of TVLA and to build up the fix set, one needs to compare all others against this data.
* `randomness` are the used maskes on the microcontroller
* `output` is self explainatory
